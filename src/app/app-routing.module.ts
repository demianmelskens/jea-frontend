import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from './components/pages/login/login.component';
import {HomeComponent} from './components/pages/home/home.component';
import {PageNotFoundComponent} from './components/pages/page-not-found/page-not-found.component';
import {LoginFormComponent} from './components/partials/login-form/login-form.component';
import {RegisterFormComponent} from './components/partials/register-form/register-form.component';
import {ProfileComponent} from './components/pages/profile/profile.component';
import {EditComponent} from './components/pages/edit/edit.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent,
    children: [
      {path: '', redirectTo: 'login', pathMatch: 'full'},
      {path: 'login', component: LoginFormComponent},
      {path: 'register', component: RegisterFormComponent},
    ]
  },
  { path: 'home', component: HomeComponent },
  { path: ':name/profile', component: ProfileComponent},
  { path: ':name/edit', component: EditComponent},
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
