// modules
import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
// components
import {AppComponent} from './app.component';
import {LoginComponent} from './components/pages/login/login.component';
import {HomeComponent} from './components/pages/home/home.component';
import {PageNotFoundComponent} from './components/pages/page-not-found/page-not-found.component';
import {KweetFeedComponent} from './components/partials/kweet-feed/kweet-feed.component';
import {MenuComponent} from './components/partials/menu/menu.component';
import {ProfileBoxComponent} from './components/partials/profile-box/profile-box.component';
import {KweetOverlayComponent} from './components/partials/kweet-overlay/kweet-overlay.component';
import {LoginFormComponent} from './components/partials/login-form/login-form.component';
import {RegisterFormComponent} from './components/partials/register-form/register-form.component';
import { ProfileComponent } from './components/pages/profile/profile.component';
import { FollowersBoxComponent } from './components/partials/followers-box/followers-box.component';
import { EditComponent } from './components/pages/edit/edit.component';
// services
import {AuthService} from './services/auth/auth.service';
import {TweetService} from './services/tweet/tweet.service';
import {UserService} from './services/user/user.service';
import {DataService} from './services/data/data.service';
// pipes
import { ReversePipe } from './pipes/reverse.pipe';
import { SearchBoxComponent } from './components/partials/search-box/search-box.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    PageNotFoundComponent,
    ReversePipe,
    KweetFeedComponent,
    MenuComponent,
    ProfileBoxComponent,
    KweetOverlayComponent,
    LoginFormComponent,
    RegisterFormComponent,
    ProfileComponent,
    FollowersBoxComponent,
    EditComponent,
    SearchBoxComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
  ],
  providers: [
    AuthService,
    TweetService,
    UserService,
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
