import {Tweet} from './tweet';

export class User {

  followers: User[];      // all users that are following the user.
  following: User[];      // all users that are following the user.
  tweets: Tweet[];        // all tweets send by the user.
  liked: Tweet[];

  constructor(
    public id: string,
    public name: string,        // name of the user
    public type: string,        // type of user
    public bio: string,         // biography that has been defined by the user.
    public location: string,    // location of the user
    public website: string,     // website link
  ) { }
}
