import {User} from './user';

export class Tweet {

  constructor(
    public id: number,
    public tweet: string,     // text of the tweet
    public sender: User,      // user which send the tweet
    public likes: User[],     // users who like the tweet
    public postedAt: string,  // date at which the tweet has been send
  ) { }
}
