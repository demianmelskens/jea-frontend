import {Component, OnInit, ViewChild} from '@angular/core';
import {TweetService} from '../../../services/tweet/tweet.service';
import {Router} from '@angular/router';
import {UserService} from '../../../services/user/user.service';
import {User} from '../../../domain/user';
import {Tweet} from '../../../domain/tweet';
import {KweetOverlayComponent} from '../../partials/kweet-overlay/kweet-overlay.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  @ViewChild(KweetOverlayComponent) overlay: KweetOverlayComponent;
  authUserTweets: Tweet[];
  allTweets: Tweet[];
  authUser: User;

  constructor(private tweetService: TweetService, private userService: UserService, private router: Router) {}

  ngOnInit() {
    if (sessionStorage.getItem('token') == null) {
      this.router.navigate(['']);
    } else {
      this.getAuthUser();
      this.getAllTweets();
    }
  }

  // Functionality methods
  getAuthUser() {
    this.userService.getAuthUser().subscribe(
      (data) => this.authUser = data,
      error => console.log(error),
      () => {
        this.getTweetsForAuthUser(this.authUser.id);
        this.userService.getFollowers(this.authUser);
        this.userService.getFollowing(this.authUser);
      }
    );
  }

  getTweetsForAuthUser(id) {
    this.tweetService.getTweetsForUser(id).subscribe(
      (data) => this.authUserTweets = data,
    );
  }

  getAllTweets() {
    this.tweetService.getAllTweets().subscribe(
      (data) => this.allTweets = data,
      (error) => console.log(error),
    );
  }

  onNotify(event) {
    switch (event) {
      case 'refresh':
        this.getAuthUser();
        this.getAllTweets();
        break;

      case 'overlay':
        this.overlay.toggleOverlay();
        break;
    }
  }
}
