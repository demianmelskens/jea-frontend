import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {User} from '../../../domain/user';
import {KweetOverlayComponent} from '../../partials/kweet-overlay/kweet-overlay.component';
import {Tweet} from '../../../domain/tweet';
import {TweetService} from '../../../services/tweet/tweet.service';
import {UserService} from '../../../services/user/user.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {

  @ViewChild(KweetOverlayComponent) overlay: KweetOverlayComponent;
  authUser: User;
  profile: User;
  profileTweets: Tweet[];
  name = '';
  isDataAvailable = false;

  constructor(
    private tweetService: TweetService,
    private userService: UserService,
    private router: Router,
    private route: ActivatedRoute) {
  }

  ngOnInit() {
    if (sessionStorage.getItem('token') == null) {
      this.router.navigate(['']);
    } else {
      this.isDataAvailable = false;
      this.route.params.subscribe(
        params => {
          this.name = params['name'];
          this.getProfile(this.name);
          this.getAuthUser();
        }
      );
    }
  }

  // Functionality methods
  getProfile(name) {
    this.userService.getUser(name).subscribe(
      (data) => this.profile = data,
      (error) => console.log(error),
      () => {
        this.getTweetsForUser(this.profile.id);
        this.userService.getFollowers(this.profile);
        this.userService.getFollowing(this.profile);
      },
    );
  }

  getAuthUser() {
    this.userService.getAuthUser().subscribe(
      (data) => this.authUser = data,
      error => console.log(error),
      () => {
        this.userService.getFollowers(this.authUser);
        this.userService.getFollowing(this.authUser);
        this.isDataAvailable = true;
      }
    );
  }

  getTweetsForUser(id) {
    this.tweetService.getTweetsForUser(id).subscribe(
      (data) => this.profileTweets = data,
    );
  }

  editProfile(user: User, name, bio, location, website) {
    user.bio = bio;
    user.location = location;
    user.website = website;
    this.userService.editUser(user).subscribe(
      (data) => this.authUser = data,
      (error) => console.log(error),
      () => this.router.navigate(['/' + name + '/profile']),
    );
  }

  onNotify(event) {
    switch (event) {
      case 'refresh':
        this.getAuthUser();
        this.getProfile(this.name);
        break;

      case 'overlay':
        this.overlay.toggleOverlay();
        break;
    }
  }
}

