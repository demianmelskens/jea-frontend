import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {TweetService} from '../../../services/tweet/tweet.service';
import {Tweet} from '../../../domain/tweet';
import {User} from '../../../domain/user';

@Component({
  selector: 'app-kweet-feed',
  templateUrl: './kweet-feed.component.html',
  styleUrls: ['./kweet-feed.component.scss']
})
export class KweetFeedComponent implements OnInit {
  @ViewChild('kweettext') el: ElementRef;
  @Output() notify: EventEmitter<string> = new EventEmitter<string>();
  @Input() authUser: User;
  @Input() tweets: Tweet[];
  @Input() myFeed: boolean;

  visibleNewKweet = false;

  constructor(private tweetService: TweetService) {}

  ngOnInit() {
  }

  // Functionality methods

  toggleLikeTweet(id, unlike) {
    if (!unlike) {
      this.tweetService.likeTweet(id, this.authUser).subscribe(
        (data) => {
        },
        (error) => console.log(error),
        () => this.notify.emit('refresh'),
      );
    } else {
      this.tweetService.unlikeTweet(id, this.authUser).subscribe(
        (data) => {
        },
        (error) => console.log(error),
        () => this.notify.emit('refresh'),
      );
    }
  }

  createTweet(event, text) {
    event.preventDefault();
    if (text !== '') {
      this.tweetService.createTweet(text, this.authUser, Date.now()).subscribe(
        (data) => console.log(data),
        (error) => console.log(error),
        () => {
          this.notify.emit('refresh');
        },
      );
    }
    this.el.nativeElement.value = '';
  }

  checkListForUser(list, id) {
    for (const user of list) {
      if (user.id = id) {
        return true;
      }
    }
    return false;
  }

  // GUI methods
  newKweetFocus() {
    this.visibleNewKweet = true;
  }

  newKweetBlur() {
    setTimeout(() => { this.visibleNewKweet = false; }, 200);
  }
}
