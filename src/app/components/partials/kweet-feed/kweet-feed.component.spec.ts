import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KweetFeedComponent } from './kweet-feed.component';

describe('KweetFeedComponent', () => {
  let component: KweetFeedComponent;
  let fixture: ComponentFixture<KweetFeedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KweetFeedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KweetFeedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
