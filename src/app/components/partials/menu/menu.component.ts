import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../../domain/user';
import {Router} from '@angular/router';
import {UserService} from '../../../services/user/user.service';
import {TweetService} from '../../../services/tweet/tweet.service';
import {Tweet} from '../../../domain/tweet';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  @Output() notify: EventEmitter<string> = new EventEmitter<string>();
  @Input() authUser: User;
  focus = '';
  visible = false;
  foundUsers: User[];

  constructor(private router: Router, private userService: UserService, private tweetService: TweetService) {
  }

  ngOnInit() {
  }

  // Functionality methods
  logout() {
    sessionStorage.removeItem('token');
    this.router.navigate(['']);
  }

  find(input) {
    if (input !== '') {
      this.userService.find(input).subscribe(
        (data) => this.foundUsers = data,
        (error) => this.foundUsers = [],
        () => console.log(this.foundUsers),
      );
    } else {
      this.foundUsers = [];
    }
  }

  // GUI methods
  searchFocus() {
    this.focus = 'focus';
  }

  searchBlur() {
    this.focus = '';
    setTimeout(() => {
      this.foundUsers = [];
      }, 500);
  }

  toggleContextMenu() {
    this.visible = !this.visible;
  }

  toggleOverlay() {
    this.notify.emit('overlay');
  }

  profileClick(event) {
    this.visible = !this.visible;
    this.router.navigate(['/' + this.authUser.name + '/profile']);
  }
}
