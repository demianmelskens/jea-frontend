import {Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {TweetService} from '../../../services/tweet/tweet.service';
import {User} from '../../../domain/user';

@Component({
  selector: 'app-kweet-overlay',
  templateUrl: './kweet-overlay.component.html',
  styleUrls: ['./kweet-overlay.component.scss']
})
export class KweetOverlayComponent implements OnInit {
  @ViewChild('newkweettext') el: ElementRef;
  @Input() authUser: User;
  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  visible = false;

  constructor(private tweetService: TweetService) {}

  ngOnInit() {
  }

  // Functionality methods
  createTweet(event, text) {
    event.preventDefault();
    if (text !== '') {
      this.tweetService.createTweet(text, this.authUser, Date.now()).subscribe(
        (data) => console.log(data),
        (error) => console.log(error),
        () => {
          this.notify.emit('refresh');
        },
      );
    }
    this.el.nativeElement.value = '';
    this.toggleOverlay();
  }

  // GUI methods
  toggleOverlay() {
    setTimeout(() => {
      this.visible = !this.visible;
    }, 100);
  }

  stopOverlayClick(event) {
    event.stopPropagation();
  }
}
