import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KweetOverlayComponent } from './kweet-overlay.component';

describe('KweetOverlayComponent', () => {
  let component: KweetOverlayComponent;
  let fixture: ComponentFixture<KweetOverlayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KweetOverlayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KweetOverlayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
