import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../domain/user';

@Component({
  selector: 'app-search-box',
  templateUrl: './search-box.component.html',
  styleUrls: ['./search-box.component.scss']
})
export class SearchBoxComponent implements OnInit {
  @Input() list: User[];

  constructor() { }

  ngOnInit() {
  }

}
