import {Component, Input, OnInit} from '@angular/core';
import {UserService} from '../../../services/user/user.service';
import {AuthService} from '../../../services/auth/auth.service';
import {DataService} from '../../../services/data/data.service';
import {Router} from '@angular/router';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {

  registerForm: FormGroup;
  username: string;
  password: string;
  accepted = false;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private dataService: DataService,
    private router: Router) { }

  ngOnInit() {
    if (this.dataService.getData() == null) {
      this.router.navigate(['/']);
    } else {
      this.username = this.dataService.getData()['username'];
      this.password = this.dataService.getData()['password'];
      this.registerForm = new FormGroup({
        name: new FormControl('', {
         validators: Validators.required,
         updateOn: 'change'
        }),
        password: new FormControl('', {
          validators: [
            Validators.required,
            Validators.minLength(8),
          ],
          updateOn: 'change',
        }),
        again: new FormControl('', {
          validators: [
            Validators.required,
            Validators.minLength(8),
          ],
          updateOn: 'change',
        }),
        accepted: new FormControl('', {
          validators: Validators.required,
          updateOn: 'change'
        }),
      });
    }
  }

  register(event, username, password, passwordConfirm) {
    event.preventDefault();
    if (password === passwordConfirm) {
      this.userService.register(username, password).subscribe(
        (data) => {},
        (error) => console.log(error),
        () => this.authService.getAuthToken(username, password),
      );
    }
  }

  toggleAccepted() {
    this.accepted = !this.accepted;
  }

}
