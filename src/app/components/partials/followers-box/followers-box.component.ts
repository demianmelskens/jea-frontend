import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../domain/user';

@Component({
  selector: 'app-followers-box',
  templateUrl: './followers-box.component.html',
  styleUrls: ['./followers-box.component.scss']
})
export class FollowersBoxComponent implements OnInit {
  @Input() followers: User[];

  constructor() { }

  ngOnInit() {
  }

}
