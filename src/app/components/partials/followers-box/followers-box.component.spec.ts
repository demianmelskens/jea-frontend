import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowersBoxComponent } from './followers-box.component';

describe('FollowersBoxComponent', () => {
  let component: FollowersBoxComponent;
  let fixture: ComponentFixture<FollowersBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FollowersBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowersBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
