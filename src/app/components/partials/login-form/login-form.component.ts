import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../../services/auth/auth.service';
import {DataService} from '../../../services/data/data.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  @Output() notify: EventEmitter<string> = new EventEmitter<string>();

  constructor(private authService: AuthService, private dataService: DataService, private router: Router) { }

  ngOnInit() {
  }

  login(event, username: string, password: string) {
    event.preventDefault();
    this.authService.getAuthToken(username, password);
  }

  register(event, username: string, password: string) {
    event.preventDefault();
    this.dataService.setData({'username': username, 'password': password});
    this.router.navigate(['/register']);
  }
}
