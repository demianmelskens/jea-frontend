import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../../domain/user';
import {Tweet} from '../../../domain/tweet';

@Component({
  selector: 'app-profile-box',
  templateUrl: './profile-box.component.html',
  styleUrls: ['./profile-box.component.scss']
})
export class ProfileBoxComponent implements OnInit {

  @Input() authUser: User;
  @Input() authUserTweets: Tweet[];

  constructor() { }

  ngOnInit() {
  }

}
