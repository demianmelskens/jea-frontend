import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../../domain/user';
import 'rxjs/add/operator/map';

const baseUrl = 'http://localhost:8080/kwetter/api';

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  register(name, password) {
    return this.http.post<User>(baseUrl + '/users', {
      'name': name,
      'password': password,
      'bio': '',
      'location': '',
      'website': '',
    });
  }

  editUser(user: User) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.put<User>(baseUrl + '/users', {data: JSON.stringify(user)}, {headers: headers});
  }

  getUser(name) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    console.log(baseUrl + '/users/name/' + name);
    return this.http.get<User>(baseUrl + '/users/name/' + name, {headers: headers});
  }

  getAuthUser() {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<User>(baseUrl + '/users/auth', {headers: headers});
  }

  getFollowers(user: User) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<User[]>(baseUrl + '/users/' + user.id + '/followers', {headers: headers}).subscribe(
      (data) => user.followers = data,
    );
  }

  getFollowing(user: User) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<User[]>(baseUrl + '/users/' + user.id + '/following', {headers: headers}).subscribe(
      (data) => user.following = data,
    );
  }

  follow(userid) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.put<User>(baseUrl + '/users/follow/' + userid, {}, {headers: headers});
  }

  unfollow(userid) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.put<User>(baseUrl + '/users/unfollow/' + userid, {}, {headers: headers});
  }

  find(name) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<User[]>(baseUrl + '/users/find?name=' + name, {headers: headers});
  }
}
