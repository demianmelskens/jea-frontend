import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Tweet} from '../../domain/tweet';
import {User} from '../../domain/user';

const baseUrl = 'http://localhost:8080/kwetter/api';

@Injectable()
export class TweetService {

  constructor(private http: HttpClient) {
  }

  createTweet(text, user, date) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.post(
      baseUrl + '/tweets',
      {
        'tweet': text,
        'sender': {
          'id': user.id, 'name': user.name, 'bio': user.bio, 'location': user.location, 'website': user.website
        }
      },
      {headers: headers}
    );
  }

  getTweetsForUser(id: number) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<Tweet[]>(baseUrl + '/tweets/users/' + id, {headers: headers});
  }

  getAllTweets() {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.get<Tweet[]>(baseUrl + '/tweets/', {headers: headers});
  }

  likeTweet(id: number, user: User) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.put<Tweet[]>(baseUrl + '/tweets/' + id + '/like/' + user.id, {}, {headers: headers});
  }

  unlikeTweet(id: number, user: User) {
    const token = sessionStorage.getItem('token');
    const headers = new HttpHeaders({'Authorization': 'Bearer ' + token});
    return this.http.put<Tweet[]>(baseUrl + '/tweets/' + id + '/unlike/' + user.id, {}, {headers: headers});
  }
}
