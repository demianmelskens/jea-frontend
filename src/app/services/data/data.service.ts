import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  data: object;

  constructor() { }

  setData(data) {
    this.data = data;
  }

  getData() {
    return this.data;
  }
}
