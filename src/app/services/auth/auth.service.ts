import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';

const baseUrl = 'http://localhost:8080/kwetter/api';

@Injectable()
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }

  getAuthToken(username: string, password: string) {
    const headers = new HttpHeaders({'Content-Type': 'application/json', 'name': username, 'password': password});
    this.http.post(baseUrl + '/users/login',  {}, { headers: headers}).subscribe(
      data => sessionStorage.setItem('token', data['token']),
      error => console.log(error),
      () => this.router.navigate(['home']),
    );;
  }
}
